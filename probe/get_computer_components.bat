@ECHO OFF

ECHO MOTHERBOARD
wmic baseboard list brief

ECHO OPERATING SYSTEM
wmic os get Caption,OSArchitecture

ECHO CPU
wmic cpu get name

ECHO RAM
wmic MEMORYCHIP get BankLabel, DeviceLocator, Capacity, Speed

ECHO GRAPHIC CARD
wmic path win32_VideoController get name

ECHO DRIVE
wmic diskdrive get model,size

PAUSE